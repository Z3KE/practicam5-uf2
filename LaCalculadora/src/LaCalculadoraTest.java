import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;


import org.junit.jupiter.api.Test;

class LaCalculadoraTest {

	@Test
	void testSuma() {
		int res = LaCalculadora.suma(10, 20);
		assertEquals(30, res);
		// fail("Not yet implemented");
	}

	@Test
	void testResta() {
		int res = LaCalculadora.resta(20, 10);
		assertEquals(10, res);
		// fail("Not yet implemented");
	}

	@Test
	void testMultiplicacio() {
		int res = LaCalculadora.multiplicacio(2, 5);
		assertEquals(10, res);
		// fail("Not yet implemented");
	}

	@Test
	void testDivisio() {
		int res = LaCalculadora.divisio(10, 2);
		assertEquals(5, res);
		// fail("Not yet implemented");
	}

	
	
	@Test
	void testExpectedException() {
		 
		 Assertions.assertThrows(ArithmeticException.class, () -> {
		     LaCalculadora.divideix(10, 0);
		  });
		 
		}


}
